#!/bin/python3

################################################################################
# IMPORTS

import os
import shutil
import argparse


################################################################################
# FILES AND FOLDER LOCATIONS

DOTFILES_FOLDER = os.path.dirname(__file__)

OLD_FILES_FOLDER = os.path.join(DOTFILES_FOLDER, "old")
FILES_FOLDER = os.path.join(DOTFILES_FOLDER, "files")

DEFAULT_CONFIG = "default"


################################################################################
# FILE LISTING COMMANDS

def list_files(folder) -> list[str]:
    files_found = list()
    for dir_path, _sub_dirs, sub_files in os.walk(folder):
        files_found.extend([
            os.path.join(dir_path, sub_file) for sub_file in sub_files
        ])
    return files_found


def list_config_files(config: str):
    config_folder = os.path.join(FILES_FOLDER, config)
    return [
        file.split(config_folder + os.sep)[1]
        for file in list_files(config_folder)
    ]


################################################################################
# USE DOTFILES

def use_dotfiles(target: str, config: str) -> None:
    destination_dir = os.path.expanduser(target)

    default_files = list_config_files(DEFAULT_CONFIG)
    specific_files = list_config_files(config)

    for file in set(default_files + specific_files):
        target_file = os.path.join(destination_dir, file)
        if os.path.exists(target_file):
            if os.path.islink(target_file):
                print(f"Removing symlink {target_file}")
                os.remove(target_file)
            else:
                old_file = os.path.join(OLD_FILES_FOLDER, file)

                old_file_folder = os.path.dirname(old_file)
                if not os.path.exists(old_file_folder):
                    print(f"Creating {old_file_folder}")
                    os.makedirs(old_file_folder, exist_ok=True)
                
                print(f"Moving old file {target_file} to {old_file}")
                shutil.move(target_file, old_file)
        
        default_file = os.path.join(FILES_FOLDER, DEFAULT_CONFIG, file)
        specific_file = os.path.join(FILES_FOLDER, config, file)
        source_file = specific_file if file in specific_files else default_file

        target_dir = os.path.dirname(target_file)
        if not os.path.exists(target_dir):
            print(f"Creating {target_dir}")
            os.makedirs(target_dir, exist_ok=True)
        
        print(f"Creating symlink {target_file} -> {source_file}")
        os.symlink(source_file, target_file)


################################################################################
# ARGUMENT PARSER

def create_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-t", "--target", dest="target",
        action="store",
        default="~", required=False,
        help="directory where the dot files will be placed (defaults to ~)",
    )
    parser.add_argument(
        "-c", "--config", dest="config",
        action="store",
        default="default", required=False,
        help="dot file configuration to use (defaults to default)",
    )

    return parser


################################################################################
# MAIN FUNCTION

def main() -> None:
    parser = create_argument_parser()
    args = parser.parse_args()
    use_dotfiles(args.target, args.config)


################################################################################
# ENTRY POINT

if __name__ == "__main__":
    main()