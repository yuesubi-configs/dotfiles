#!/usr/bin/python

import json
import subprocess


nmcli = subprocess.Popen(
    ["wpctl", "get-volume", "@DEFAULT_AUDIO_SINK@"],
    stdout=subprocess.PIPE, text=True
)
nmcli.wait()

text = nmcli.stdout.readline()

collected = {
    "mut": len(text) > 15,
    "vol": float(text[8:12])
}

print(json.dumps(collected))
