#!/usr/bin/python

import json
import subprocess


nmcli = subprocess.Popen(
    ["nmcli", "-g", "TYPE,STATE", "device", "status"],
    stdout=subprocess.PIPE,
    text=True
)
nmcli.wait()

collected = {
    "wifi": False,
    "ethernet": False
}

for line in nmcli.stdout.readlines():
    values = line.split(':')
    type_ = values[0]

    if type_ in collected.keys():
        state = values[1].rstrip(('\n'))
        collected[type_] = state == "connected"

print(json.dumps(collected))