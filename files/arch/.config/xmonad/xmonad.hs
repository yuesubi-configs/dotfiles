----------------------------------------
-- IMPORTS

import XMonad

--import XMonad.Hooks.DynamicLog
--import XMonad.Hooks.StatusBar
--import XMonad.Hooks.StatusBar.PP

import XMonad.Util.EZConfig
import XMonad.Util.Ungrab


----------------------------------------
-- ENTRY POINT

main :: IO()
main = xmonad $ myConfig

----------------------------------------
-- VISUAL APPEARANCE

borderWidth :: Dimension
borderWidth = 2


----------------------------------------
-- CONFIGURATION

myConfig = def
	{
	-- Use the super key as the mod mask
	modMask = mod4Mask
	}
	`additionalKeysP`
	[ ("M-f", spawn "nemo")
	, ("M-w", spawn "firefox")
	]
