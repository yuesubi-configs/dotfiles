################################################################################
# IMPORTS

from libqtile import bar, layout, hook, widget
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

#from qtile_extras import widget
#from qtile_extras.widget.decorations import RectDecoration

import subprocess


################################################################################
# VARIABLES

mod = "mod4"
terminal = "alacritty"


################################################################################
# COLORS

colors: dict[str, str] = {
    "Background": 	"#282a36",
    "Current Line": "#44475a",
    "Selection":    "#44475a",
    "Foreground": 	"#f8f8f2",
    "Comment": 	    "#6272a4",
    "Cyan": 	    "#8be9fd",
    "Green": 	    "#50fa7b",
    "Orange": 	    "#ffb86c",
    "Pink": 	    "#ff79c6",
    "Purple": 	    "#bd93f9",
    "Red": 	        "#ff5555",
    "Yellow": 	    "#f1fa8c",
}


################################################################################
## STARTUP PROGRAMS

@hook.subscribe.startup_once
def startup_programs() -> None:
    program_commands = [
        ["picom"],  # Compositor
        ["nitrogen", "--restore"],  # Wallpaper
        #["volumeicon"],
        ["nm-applet"],  # Network manager applet
        ["numlockx", "on"],  # Enable numlock
        ["/usr/bin/dunst"],  # Notification server
    ]

    for program_command in program_commands:
        subprocess.Popen(program_command)


keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "p", lazy.spawn("dmenu_run"), desc="Spawn a command using a prompt widget"),
]


################################################################################
# GROUPS / WORKSPACES

group_keys = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "minus", "egrave", "underscore", "ccedilla"]
#group_names = ["bin", "dev", "etc", "mnt", "opt", "run", "sys", "usr", "var"]
group_names = ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十"]

groups = [Group(name) for name in group_names]

# Key bindings to switch workspace
keys.extend([
    Key(
        [mod],
        key,
        lazy.group[group.name].toscreen(),
        desc=f"Switch to group {group.name}",
    )
    for key, group in zip(group_keys, groups)
])

# Keychord to switch workspace and send windows to different ones
keys.extend([
    KeyChord([mod], "w", [
        Key(
            [],
            key,
            lazy.group[group.name].toscreen(),
            desc=f"Switch to group {group.name}",
        )
        for key, group in zip(group_keys, groups)
    ]),
    KeyChord([mod, "shift"], "w", [
        Key(
            [],
            key,
            lazy.window.togroup(group.name, switch_group=True),
            desc=f"Switch to & move focused window to group {group.name}",
        )
        for key, group in zip(group_keys, groups)
    ])
])


layouts = [
    layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=2, margin=2),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="Fira Code",
    fontsize=12,
    padding=3,
    background=colors["Background"],
    foreground=colors["Foreground"],
)
extension_defaults = widget_defaults.copy()


def get_widgets(include_systray: bool = False) -> list:
    widget_list = []
    widget_list.extend([
        widget.GroupBox(
            highlight_method="line",
            highlight_color=[colors["Background"], colors["Background"]],
            foreground=colors["Pink"],
            inactive=colors["Comment"],
            urgent_alert_method="line",
            urgent_border=colors["Red"],
            fontsize=14
        ),
        widget.CurrentLayout(),
        widget.WindowName(
            background=colors["Current Line"],
            font="sans"
        ),
        widget.Chord(
            chords_colors={
                "launch": ("#ff0000", "#ffffff"),
            },
            name_transform=lambda name: name.upper(),
        ),
        # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
        # widget.StatusNotifier(),
        
        # System activity information
        widget.CPU(
            format="CPU {load_percent}% ",
            foreground=colors["Foreground"],
            #decorations=[
            #    RectDecoration(
            #        colour=colors["Cyan"],
            #        radius=10,
            #    ),
            #]
        ),
        widget.TextBox(
            "\ue0b6",
            background="#ff0000",
            foreground="#00ff00",
            fontsize=21,
            padding=0,
        ),
        widget.Memory(
            format="RAM {MemUsed: .01f}{mm}/{MemTotal: .01f}{mm} ",
            measure_mem='G',
            foreground=colors["Orange"],
            background="#ffffff",
        ),
        widget.Net(
            format="NET {up} {down}",
            foreground=colors["Pink"],
        ),
    ])

    if include_systray:
        widget_list.append(
            widget.systray.Systray()
        )
    
    widget_list.extend([
        widget.Clock(
            format="%a %d/%m %H:%M",
            foreground=colors["Purple"],
        ),
    ])

    return widget_list


SCREEN_AMOUNT = int(subprocess.run(["xrandr", "--listmonitors"], capture_output=True).stdout.decode()[10])

screens = [
    Screen(
        top=bar.Bar(
            get_widgets(i == 0),
            24,
            border_width=2,  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    )
    for i in range(SCREEN_AMOUNT)
]


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
