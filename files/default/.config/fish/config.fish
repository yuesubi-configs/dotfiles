export (envsubst < ~/.config/fish/env)

if status is-interactive
    # Commands to run in interactive sessions can go here

    # Path
    fish_add_path .local/bin    

    # Aliases
    alias ls="exa -lh"
    alias la="exa -lhagH"
    alias lsblk="lsblk -f -o NAME,FSTYPE,LABEL,SIZE,FSAVAIL,MOUNTPOINTS"

    alias vim="nvim"
    
    alias grep="grep --color=auto"

    starship init fish | source

    cowsay $(fortune -s)
end

# opam configuration
source /home/arch/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true
